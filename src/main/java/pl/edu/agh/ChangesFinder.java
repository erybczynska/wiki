package pl.edu.agh;

import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ChangesFinder {

  private static final Pattern PATTERN = Pattern
      .compile("loki:valueBefore \"(\\d*\\.?\\d*)\"; loki:valueAfter \"(\\d*\\.?\\d*)\"");

  public static void main(String[] args) throws IOException {
    Path rootDir = Paths.get("/Users/ewelinarybczynska/Downloads/prov");

    try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(rootDir)) {
      List<Pair<String>> pairs = StreamSupport
          .stream(dirStream.spliterator(), false)
          .map(ChangesFinder::handleDirectoryStream)
          .flatMap(Collection::stream)
          .collect(toList());

      pairs
          .forEach(pair -> System.out.println(pair.before + ", " + pair.after + " : " + pair.line));
      System.out.println(pairs.size());
    }
  }

  private static List<Pair<String>> handleDirectoryStream(Path path) {
    try {
      if (Files.isDirectory(path)) {
        return handleDirectory(path);
      } else {
        return handleTtlFile(path);
      }
    } catch (Exception e) {
      e.printStackTrace();
      return Collections.emptyList();
    }
  }

  private static List<Pair<String>> handleTtlFile(Path path) {
    try {

      return Files.lines(path).map(line -> {
        Matcher matcher = PATTERN.matcher(line);
        if (matcher.find()) {
          return new Pair<>(matcher.group(1), matcher.group(2), path, line);
        } else {
          return null;
        }
      }).filter(Objects::nonNull)
//          .filter(pair -> !pair.before.equals(pair.after))
          .collect(Collectors.toList());

    } catch (IOException e) {
      return Collections.emptyList();
    }
  }

  private static List<Pair<String>> handleDirectory(Path path) {
    try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(path)) {
      return StreamSupport
          .stream(dirStream.spliterator(), false)
          .map(ChangesFinder::handleDirectoryStream)
          .flatMap(Collection::stream)
          .collect(Collectors.toList());
    } catch (IOException e) {
    }
    return Collections.emptyList();
  }

  private static class Pair<T> {

    private T before;
    private T after;
    private Path path;
    private String line;

    Pair(T before, T after, Path path, String line) {
      this.before = before;
      this.after = after;
      this.path = path;
      this.line = line;
    }
  }
}