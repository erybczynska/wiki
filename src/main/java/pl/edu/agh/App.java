package pl.edu.agh;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.memory.model.MemIRI;
import org.eclipse.rdf4j.sail.memory.model.MemLiteral;

public class App {

  private static final String DB_PATH = "database";
  private static final String INPUT_FILE = "start2.ttl";

  private static final String LOKI_PREFIX = "http://loki.ia.agh.edu.pl/wiki/ns#";

  private static final String WEIGHTED_AVERAGE_QUERY = "PREFIX loki: <" + LOKI_PREFIX + ">"
      + "SELECT ?page ?value \n"
      + "WHERE {\n"
      + "   ?page loki:weightedAverage ?value."
      + "}";

 private static final String TEST_PASSED_VALUE_BEFORE_QUERY = "PREFIX loki:  <" + LOKI_PREFIX + ">\n"
     + "SELECT ?value ?page \n"
     + "WHERE {\n"
     + "    ?page loki:testsPassed ?node .\n"
     + "    ?node loki:valueBefore ?value .\n"
     + "}";

  private static final String TEST_PASSED_VALUE_AFTER_QUERY = "PREFIX loki:  <" + LOKI_PREFIX + ">\n"
      + "SELECT ?value ?page \n"
      + "WHERE {\n"
      + "    ?page loki:testsPassed ?node .\n"
      + "    ?node loki:valueAfter ?value .\n"
      + "}";

  public static void main(String[] args) throws IOException {
    Repository db = new SailRepository(new MemoryStore(new File(DB_PATH)));
    db.initialize();

    try (RepositoryConnection connection = db.getConnection()) {
      handleConnection(connection);
    } finally {
      db.shutDown();
    }
  }

  private static void handleConnection(RepositoryConnection connection) throws IOException {
    updateModel(connection, INPUT_FILE);

    try (RepositoryResult<Statement> result = connection.getStatements(null, null, null)) {
      while (result.hasNext()) {
        Statement st = result.next();
        System.out.println("db contains: " +
            st.getSubject() + ' ' + st.getPredicate() + ' ' + st.getObject());
      }
    }
  }

  private static void updateModel(RepositoryConnection connection, String fileName)
      throws IOException {
    InputStream input = App.class.getClassLoader().getResourceAsStream(fileName);
    Model model = Rio.parse(input, "", RDFFormat.TURTLE);
    connection.clear();
    connection.add(model);
    executeQuery(WEIGHTED_AVERAGE_QUERY, connection);
    executeQuery(TEST_PASSED_VALUE_AFTER_QUERY, connection);
    executeQuery(TEST_PASSED_VALUE_BEFORE_QUERY, connection);
  }

  private static void executeQuery(String query, RepositoryConnection connection) {
    try (TupleQueryResult result = connection.prepareTupleQuery(query).evaluate()) {
      while (result.hasNext()) {
        BindingSet solution = result.next();
        MemIRI page = (MemIRI) solution.getValue("page");
        MemLiteral value = (MemLiteral) solution.getValue("value");

        System.out.println("?page = " + page);
        System.out.println("?value = " + value.getLabel());
      }
    }
  }
}
